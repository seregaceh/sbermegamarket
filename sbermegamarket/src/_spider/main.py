import asyncio
import random

from playwright.async_api import async_playwright
from logger import tracking_request
from playwright.async_api import Page
from settings import constant
from state import JsonFileStorage, State
from settings import settings


async def fetch_sub_category(page: Page, category: str = '0', chunks: int = 10):
    url = f"http://127.0.0.1:8000/sbermegamarket/category/{category}"
    await page.goto(url)
    async with page.expect_response(constant.category_url, timeout = 5 * 1000) as response:
        response = await response.value
        data = await response.json()
        category = list(filter(lambda d: d['parentId'] != category, data.get('nodes')))
        for i in range(0, len(category), chunks):
            yield category[i:i + chunks]



async def fetch_items_by_category(collection: dict, page: Page, limit: int = 44, offset: int = 0):

    title = collection.get('title')
    collection_id = collection.get('collectionId')
    page.on("request", tracking_request)

    await page.goto(
        f"http://127.0.0.1:8000/sbermegamarket/search/{collection_id}?limit={limit}&offset={offset}"
    )

    async with page.expect_response(constant.search_url) as response:
        response = await response.value
        data = await response.json()
        return title, {
            'collection_id': collection_id,
            'total': data.get('total')
        }



async def fetch(categories, page, state):

    for category in categories:
        collection = category.get('collection')
        key, value = await fetch_items_by_category(collection, page)
        state.set_state(key, value)
    await page.close()


async def bound_fetch(semaphore, categories, browser, state):
    async with semaphore:
        await fetch(categories, await browser.new_page(), state)
    await asyncio.sleep(random.randint(7, 10))


async def main():

    start_category = '0'
    storage = JsonFileStorage(settings.LocalStorage)
    state = State(storage)

    async with async_playwright() as p:
        browser = await p.firefox.launch(headless = True)
        page = await browser.new_page()
        page.on("request", tracking_request)

        total_chunk = 10
        semaphore = asyncio.Semaphore(total_chunk)

        tasks = []
        async for categories in fetch_sub_category(page, start_category, chunks = total_chunk):
            tasks.append(asyncio.ensure_future(bound_fetch(semaphore, categories, browser, state)))
        responses = asyncio.gather(*tasks)
        await responses

        await browser.close()


asyncio.run(main())
