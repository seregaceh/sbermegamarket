import asyncio
import random

from logger import tracking_request
from playwright.async_api import Page
from settings import constant
from state import JsonFileStorage, State
from settings import settings
from playwright.async_api import async_playwright


async def fetch_items_by_category(collection_id: str, page: Page, limit: int = 44, offset: int = 0):
    page.on("request", tracking_request)
    await page.goto(
        f"http://web:8000/sbermegamarket/search/{collection_id}?limit={limit}&offset={offset}"
    )
    async with page.expect_response(constant.search_url) as response:
        response = await response.value
        data = await response.json()
        return data


async def fetch(categories, page, state):
    for category in categories:
        collection = category.get('collection')
        key, value = await fetch_items_by_category(collection, page)
        state.set_state(key, value)
    await page.close()


async def bound_fetch(semaphore, categories, browser, state):
    async with semaphore:
        await fetch(categories, await browser.new_page(), state)
    await asyncio.sleep(random.randint(7, 10))


async def main():
    storage = JsonFileStorage(settings.LocalStorage)
    state = State(storage)

    category = state.get_state('Кондитерские изделия')

    collection_id = category.get('collection_id')
    total = int(category.get('total'))

    async with async_playwright() as p:
        browser = await p.firefox.launch(headless = True)
        page = await browser.new_page()
        page.on("request", tracking_request)

        for offset in range(0, total, 44):
            data = await fetch_items_by_category(collection_id, page, limit = 44, offset = offset)
            for item in data.get('items'):
                print(item)


asyncio.run(main())
