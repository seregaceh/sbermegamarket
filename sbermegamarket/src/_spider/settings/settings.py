import os
from pydantic import BaseSettings, HttpUrl
from pathlib import Path

BASE_DIR = Path(os.path.dirname(os.path.dirname(__file__)))


class Settings(BaseSettings):

    LocalStorage = BASE_DIR / 'storage.json'
    semaphore: int = 50

