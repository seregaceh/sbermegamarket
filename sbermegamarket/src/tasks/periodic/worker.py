
from typing import Any
from arq.cron import cron
from spider.base import Spider
from logger import tracking_request
from playwright.async_api import Page
from state import State
from settings import settings

from datetime import datetime


async def startup(ctx: dict[str, Any], spider: Spider = Spider()) -> None:
    await spider.create()
    ctx["browser"] = spider.browser


async def shutdown(ctx: dict[str, Any]) -> None:
    browser = ctx["browser"]
    await browser.close()


async def fetch_sub_category(page: Page, category: str = '0'):
    url = f"http://127.0.0.1:8000/sbermegamarket/category/{category}"
    await page.goto(url)
    async with page.expect_response(settings.constant.category_url, timeout = 5 * 1000) as response:
        response = await response.value
        data = await response.json()
        for category in list(filter(lambda d: d['parentId'] != category, data.get('nodes'))):
            yield category.get('collection')


async def run_regularly(ctx):
    start_category = '0'

    browser = ctx["browser"]
    page = await browser.new_page()
    page.on("request", tracking_request)

    state = State(settings.storage)

    async for collection in fetch_sub_category(page, start_category):

        title = collection.get('title')
        collection_id = collection.get('collectionId')
        state.set_state(
            collection_id, {'name': title, 'last_upadte': datetime.now()}
        )

    await page.close()


# arq tasks.periodic.worker.WorkerUpdateCategory
class WorkerUpdateCategory:
    on_startup = startup
    on_shutdown = shutdown
    cron_jobs = [
        cron(run_regularly, second = {10})
    ]
